﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace Storytime.MVVM
{
    public class MainPageVM : BaseVM
    {
        public MainPageVM()
        {
            IncreaseCommand = new Command(() => Number++);
        }

        private int _number = 0;
        public int Number
        {
            get => _number;
            set => setProperty(ref _number, value);
        }

        public string HelloWorld { get; set; } = "Hello world!";
        public ICommand IncreaseCommand { get; set; }

        private string _customText;

        public string CustomText
        {
            get => _customText;
            set => setProperty(ref _customText, value);
        }
    }
}
